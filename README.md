# Neural Networks in JavaScript

[Making AI amazing in JavaScript & Node.js](https://github.com/liquidcarrot/nn/wiki/Creating-Liquid-Carrot)

## Roadmap

* [x] Developer SWAG! - _"just be cool, BRO! o.O"_ - **Finished: 07/01/2019**

* [x] Node.js Support (Dynamic Exporting) - _"do things offline"_ - **Finished: 07/01/2019**
* [x] Browser Support (Dynamic Exporting) - _"do things online"_ - **Finished: 07/01/2019**

* [x] Node.js Support (Dynamic Importing) - _"play with other people's toys offline"_ - **Finished: 07/01/2019**
* [x] Browser Support (Dynamic Importing) - _"play with other people's toys offline"_ - **Finished: 07/01/2019**

* [x] Threading (Node) - _"a few things at once"_ - **Finished: 07/01/2019**
* [x] Threading (Browser) - _"a few things at once"_ - **Finished: 07/05/2019**

* [x] Unit Tests (Node) - _"being safe, rather than sorry"_ - **Finished: 07/05/2019**
* [x] Unit Tests (Browser) - _"being safe, rather than sorry"_ - **Finished: 07/05/2019**

* [ ] Streaming (Node) - _"bigger data with less computer"_
* [ ] Streaming (Browser) - _"bigger data with less computer"_

* [ ] End-to-End Tests - _"being seriously, over-the-top safe - some might even say neurotic"_

* [ ] GPU (Node) - _"a lot of things at once"_
* [ ] GPU (Browser) - _"a lot of things at once"_

* [ ] Supervised Learning - _"if it's hot, don't touch it"; "got you, okay"_
* [ ] Evolutionary Learning - _100 networks start; 3 "make it"; 30 die; the remaining 50 procreate and replace the 30 deaths; 16 of the original 100 are cloned/revived_
* [ ] Transfer Learning - _copy/paste & re-train networks_
* [ ] Mimicry Learning - _observe replicate other network or entity_
* [ ] Reinforcement Learning - _touch something and it explodes - "let's not"; touch something else and it drops gold bricks - "ooh shiny"_

* [ ] Generative Adverserial Networks - _"devil's advocate networks"_
* [ ] Capsule Networks - _"I heard you like brains, so I put a brain in your brain, in your brain - in your brain"_
* [ ] HyperNEAT - _"puberty for brains - just with better outlooks"_
